package youtubeTest

import "fmt"

func Test() {
	array1 := [3]int{}
	fmt.Println(array1)

	array2 := [...]int{}
	fmt.Println(array2)

	array3 := [...]int{2: 2, 3: 4}
	fmt.Println(array3)
}

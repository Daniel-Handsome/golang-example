package youtubeTest

import "fmt"

type ninjaStar struct {
	owner string
}

func (ninjaStar) attack() {
	fmt.Println("ninjaStar")
}

func (ninjaStar) pick() {
	fmt.Println("ninjaStar pick!!!!")
}

type ninjaSword struct {
	owner string
}

func (ninjaSword) attack() {
	fmt.Println("ninjaSword")
}

type ninjaWeapon interface {
	attack()
}

func attack(weapon ninjaWeapon) {
	weapon.attack()
}

func interFace() {
	weapons := []ninjaWeapon{ninjaStar{"daniel"}, ninjaSword{"daniel"}}
	for _, weapon := range weapons {
		weapon.attack()
		switch weapon.(type) {
		case ninjaStar :
			weapon.(ninjaStar).pick()
		}
	}
}
package youtubeTest

import (
	"encoding/json"
	"fmt"
)

type Ninja struct {
	Name   string `json:"full_name"`
	Weapon Weapon `json:"weapon"`
	Leavel int `json:"leavel"`
}

type Weapon struct {
	Name string `json:"weapon_name"`
	Leavel int `json:"weapon_leavel"`
}


func jsonFormat() {
	response := `{ "full_name" : "daniel", "weapon" : {"weapon_name" : "Ninja Star", "weapon_leavel" : 6}, "leavel" : 1 }`

	var daniel Ninja
	err := json.Unmarshal([]byte(response), &daniel)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(daniel)


	marshalString, err := json.Marshal(daniel)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("%T\n", marshalString)
	fmt.Printf("%s\n", marshalString)
}
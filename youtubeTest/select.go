package youtubeTest

import (
	"fmt"
)

func Select() {
	ninja1, ninja2 := make(chan string), make(chan string)

	go captainElect(ninja1, "one")
	go captainElect(ninja2, "two")

	select {
	case message := <- ninja1:
			fmt.Println(message)
	case message := <- ninja2:
			fmt.Println(message)
	default:
			fmt.Println("default")
	}
	roughlyFair()
}

func captainElect(ninja chan<- string, message string) {
	ninja <- message
}

func roughlyFair() {
	ninja1 := make(chan interface{}); close(ninja1)
	ninja2 := make(chan interface{}); close(ninja2)

	var ninja1Count, ninja2Count int
	for i := 0; i < 1000; i++ {
		select {
		case <-ninja1:
			ninja1Count++
		case <-ninja2:
			ninja2Count++
		}
	}

	fmt.Printf("count1: %d, count2: %d", ninja1Count, ninja2Count)
}


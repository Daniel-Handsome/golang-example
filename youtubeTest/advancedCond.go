package youtubeTest

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func RunCond() {
	// getReadyCond()
	Broadcast()
}

func Broadcast() {
	fmt.Println("start")

	cond := sync.NewCond(&sync.Mutex{})
	var wg sync.WaitGroup
	wg.Add(3)

	startByMission(func ()  {
		wg.Done()
		fmt.Println("first")
	}, cond)


	startByMission(func ()  {
		wg.Done()
		fmt.Println("two")
	}, cond)

	
	startByMission(func ()  {
		wg.Done()
		fmt.Println("three")
	}, cond)

	cond.Broadcast()
	wg.Wait()
	fmt.Println("ok")
}

func startByMission(fn func(), broadcast *sync.Cond) {
	var wg sync.WaitGroup
	wg.Add(1)

	go func ()  {
		wg.Done()
		broadcast.L.Lock()
		defer broadcast.L.Unlock()

		broadcast.Wait()
		fn()
	}()
	wg.Wait()
}


var ready bool

func getReadyCond() {	
	cond := sync.NewCond(&sync.Mutex{})
	go getReady(cond)
	var work int
	
	cond.L.Lock()
	for !ready {
		work++
		cond.Wait()
	}
	cond.L.Unlock()

	fmt.Println(work)
}

func getReady(cond *sync.Cond) {
	sleep()
	ready = true
	cond.Signal()
}

func sleep() {
	rand.Seed(time.Now().UnixMicro())
	someTime := time.Duration(1 + rand.Intn(5)) * time.Second
	time.Sleep(someTime)
}
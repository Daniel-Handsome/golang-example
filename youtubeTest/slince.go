package youtubeTest

import "fmt"

func sliceTest() {
    a := []string{"Hello1", "Hello2", "Hello3"}
    fmt.Println(a, a[:0], a[0])
    // [Hello1 Hello2 Hello3]
    a = append(a[:0], a[1:]...)
    fmt.Println(a)
    // [Hello2 Hello3]

    s := []int{1, 2, 3, 4, 5}

    // over the slice capacity will cause runtime panic
    overSliceCap := s[0:6]
    fmt.Println(overSliceCap)
}
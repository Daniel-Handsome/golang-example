package youtubeTest

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func AtomicTest() {
	var diffSum int64
	fmt.Println(atomic.LoadInt64(&diffSum))
	atomic.StoreInt64(&diffSum, 5)
	fmt.Println(diffSum)

	var v atomic.Value
	daniel := ninja{ name: "Daniel" }
	v.Store(daniel)	
	var wg sync.WaitGroup
	wg.Add(1)

	go func ()  {
		danielNinja := v.Load().(ninja)
		danielNinja.name = "not ninja"
		v.Store(danielNinja)
		wg.Done()
	}()
	
	wg.Wait()

	fmt.Println(v.Load())
}

type ninja struct {
	name string
}
package youtubeTest

import (
	"fmt"
	"strings"
)

func stringTest() {
	s := "hello world"
	fmt.Println(s)
	fmt.Println(s[0])
	fmt.Printf("%c", s[0])
	fmt.Println()
	fmt.Println(s[:3])

	fmt.Println(s, []byte(s))


	fmt.Println(strings.ToUpper(s))
}
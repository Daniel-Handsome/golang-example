package youtubeTest

import (
	"fmt"
	"strconv"
)

func youtubeError() {
	result, err := returnError(true)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(result)
	}
}

type CustomerError struct {
	errorCode    int
	errorMessage string
}

func (c CustomerError) Error() string {
	return c.errorMessage + strconv.Itoa(c.errorCode)
}

func returnError(isError bool) (string, error) {

	if isError {
		return "bad", CustomerError{errorCode: 201, errorMessage: "錯誤"}
	}

	return "good", nil
}

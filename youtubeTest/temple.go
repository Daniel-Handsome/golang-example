package youtubeTest

import (
	"fmt"
	"os"
	"text/template"
)


type secret struct {
	Name string
	Secret string
}

func templateTest()  {
	
	secret := secret{Name: "test name", Secret: "test secret"}

	templatePath := "C:\\Users\\88697\\Desktop\\go\\yt\\temple.txt"

	t, err := template.New("temple.txt").ParseFiles(templatePath)

	if err != nil {
		fmt.Println(err)
	}

	err = t.Execute(os.Stdout, secret)
	if err != nil {
		fmt.Println(err)
	}
}
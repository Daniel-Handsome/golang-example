package youtubeTest

import (
	"context"
	"reflect"
	"sync"
	"sync/atomic"
	"time"
)

// 定時頻道操作
func ToCannelTimedContext(ctx context.Context,d time.Duration,message interface{},c chan<-interface{}) (writer bool) {
			ctx, cancel := context.WithTimeout(ctx, d)
			defer cancel()
			select {
			case c<-message:
				return true
			case <-ctx.Done():
				return false
			}
}

// 先到先得
// 有時您想將相同的消息寫入多個通道，先寫入可用的通道，但不要在同一通道上兩次寫入相同的消息。

// 為此，有兩種方法：您可以使用局部變量屏蔽通道，並select相應地禁用這些情況，或者使用 goroutine 和等待。
func FirstComeFirstServedSelect(message interface{}, a,b chan<-interface{}) {
	for i := 0; i < 2; i++ {
		select {
		case a<-message:
				a = nil
		case b<-message:
				b = nil
		}
	}
}

func FirstComeFirstServedGoroutines(message interface{}, a,b chan<-interface{}) {
	var wg sync.WaitGroup
	wg.Add(2)
	go func ()  { a<-message; wg.Done()}()
	go func ()  { b<-message; wg.Done()}()
	wg.Wait()
}

//  ### 如果您的代碼在您審查後仍然存在並且仍然有未綁定的移動部件，那麼這裡有兩種解決方案來支持它：
func FirstComeFirstServedGoroutinesVariadic(message interface{}, chs ...chan<- interface{}) {
	var wg sync.WaitGroup
	wg.Add(len(chs))
	for _, c := range chs {
		c := c
		go func() { c <- message; wg.Done() }()
	}
	wg.Wait()
}

func FirstComeFirstServedSelectVariadic(message interface{}, chs ...chan<- interface{}) {
	cases := make([]reflect.SelectCase, len(chs))
	for i, ch := range chs {
		cases[i] = reflect.SelectCase{
			Dir:  reflect.SelectSend,
			Chan: reflect.ValueOf(ch),
			Send: reflect.ValueOf(message),
		}
	}
	for i := 0; i < len(chs); i++ {
		chosen, _, _ := reflect.Select(cases)
		cases[chosen].Chan = reflect.ValueOf(nil)
	}
}

// ## 如果您想嘗試多次發送一段時間並在發送時間過長時中止，這裡有兩種解決方案：一種使用time+ select，另一種使用context+ go
// Put it together

func ToCannelTimedContextGoroutines(ctx context.Context, d time.Duration, message interface{}, ch ...chan interface{}) (writer int) {
	ctx, cancel := context.WithTimeout(ctx, d)
	defer cancel()

	var (
		wr int32
		wg sync.WaitGroup
	)

	wg.Add(len(ch))

	for _, c := range ch {
		// https://www.gushiciku.cn/pl/2eCy/zh-tw
			c := c  
			go func ()  {
				defer wg.Done()

				select {
				case c<-message :
					atomic.AddInt32(&wr, 1)
				case <-ctx.Done():
				}
			}()
	}

	wg.Wait()

	return int(wr)
}
package youtubeTest

import (
	"fmt"
	"time"
)

func channel() {
	now := time.Now()
	defer func() {
		fmt.Println(time.Since(now))
	}()

	smokeSignal := make(chan bool)

	evilNinja := "Tommy"

	go channelAttack(evilNinja, smokeSignal)
	fmt.Println(<-smokeSignal)
}

func channelAttack(target string, attacked chan bool) {
	time.Sleep(time.Second)
	fmt.Println(target + " " + "is attacked")
	attacked <- true
}

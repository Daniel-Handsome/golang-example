package youtubeTest

import "fmt"

func structTest() {
	type ninja struct {
		name    string
		weapons []string
		leavel  int
	}

	wallace := ninja{name: "Wallace"}
	fmt.Println(wallace)

	wallace = ninja{name: "wallace"}
	fmt.Println(wallace)


	type dojo struct {
		name string
		ninja ninja
	}

	golangDojo := dojo{
		name: "golangDojo",
		ninja: wallace,
	}
	fmt.Println(golangDojo)

	golangDojo.ninja.leavel = 1
	fmt.Println(golangDojo)
	fmt.Println(wallace.leavel)
	// user walce leavel is old

	type addressDojo struct {
		name string
		ninja *ninja
	}

	addressed := addressDojo{name: "addressDojo", ninja: &wallace}

	fmt.Println(addressed);
	fmt.Println(*addressed.ninja);
	fmt.Println(addressed.ninja.name);

	addressed.ninja.name = "Test"
	fmt.Println(*addressed.ninja, wallace);

	intern := ninjaIntern{name: "intern", leavel: 1}
	fmt.Println(intern)
	intern.sayHello()
}

type ninjaIntern struct {
	name string
	leavel int
}

func (n ninjaIntern) sayHello()  {
	fmt.Println(n.name)
}
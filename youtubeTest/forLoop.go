package youtubeTest

import (
	"fmt"
)

func  forLoop()  {
	s := "hello world"
	fmt.Println(s)

	// for i := 0;  i < len(s);  i++ {
	// 	// fmt.Println(i)
	// 	// fmt.Println(s[i])
	// 	fmt.Printf("%c", s[i])
	// 	fmt.Println()
	// }

	for _, value := range s {
		fmt.Printf("%c", value)

	}
}